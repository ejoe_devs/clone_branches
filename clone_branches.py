#!/usr/bin/python3
import os
from threading import Thread


class CloneBranch(Thread):

    def __init__(self, even):
        self.file_name = 'branches.txt'
        self.even = even
        self.count = line_count(self.file_name)
        Thread.__init__(self)

    def run(self):
        count = 0
        with open(self.file_name, 'r') as f:
            for line in f:
                if self.even == is_even(count):
                    print(f'{Thread.getName(self)} - Cloning {line}')
                    clone_branch(line)
                    print(f'{Thread.getName(self)} - Finished cloning {line}')
                count += 1


def clone_branch(repo):
    os.system(f'git clone --single-branch --branch 12.0 {repo}')


def line_count(file_name):
    count = 0
    with open(file_name, 'r') as f:
        for i in enumerate(f):
            count += 1
    return count


def is_even(n):
    return n % 2 == 0


thread1 = CloneBranch(True)
thread1.setName('Thread 1')
thread1.start()
thread2 = CloneBranch(False)
thread2.setName('Thread 2')
thread2.start()
