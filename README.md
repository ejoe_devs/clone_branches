**Clone Branches**

Script de Python para clonar las ramas de OCA de Odoo 12. Descarga los repositorios que se hayan incluido en el fichero branches.txt (se incluye en el repositorio sample.branches.txt como ejemplo).

*Pendiente agregar varias funcionalidades, como la posibilidad de configurar el script mediante un fichero .env*

---
